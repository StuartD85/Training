module.exports = {
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered static site generator running on GitLab Pages',
    base: '/Training/',
    dest: 'public'
}