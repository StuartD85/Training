---
home: true
heroImage: /hero.png
features:
- title: Simplicity First
  details: Minimal setup with markdown-centered project structure helps you focus on writing.
- title: Vue-Powered
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: Performant
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: MIT Licensed | Copyright © 2018-present Evan You
---
### Learning Platforms / Partners:
* Infinite Skills > O'Reilly
* Pearson > LiveLessons
* Packt Publishing > Packt
* Stone River eLearning
* CBT Nuggets
* Pluralsight
* Udemy > Varied
* VueSchool / VueSchool.io
* iCollege
* ITProTV
* Intellezy
* iNE
* LevelUP > Udemy
* LinuxAcademy
* ITFlee.com > Udemy
* Apress
* ACloudGuru
* EC Council
* CodeWithMosh > Udemy
* CodeMy.com > Udemy
* Educator > Udemy
* DataCumulus > Udemy
* eLearnSecurity
* Frontend Masters
* eWPTX
* Lynda
* Pentester Academy
* Pirple > Udemy
* Skillshare (Similar to Udemy)
* Real Python
* SANS
* SyberOffense
* TeamTreeHouse
* Technics Publications
* VTC
* UpDegree
* Wiley
* WintellectNow
* Xpertskills